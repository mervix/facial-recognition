/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const process = /* GraphQL */ `
  query Process($imageName: String!, $type: String) {
    process(imageName: $imageName, type: $type) {
      results
    }
  }
`;
