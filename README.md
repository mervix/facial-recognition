# RekognizeMe 
We are using [Amazon Rekognition](https://aws.amazon.com/rekognition/) software to authenticate attendees to events and/or take roll.

# Features
A variety of useful features may be put in this project

* Recognizing a person's face and comparing it to a database

* Recognizing emotion

* Measuring (active) employee diversity ???

## Use Cases
Examples:

* Authenticate employees who wish to access a building, deny others.

* School taking roll for classes.

* Registering attendees at public events.

# Directory Structure
**frontend:** holds our frontend (amplify) code.

**backend:** Stores our code.

**database:** Stores our total pictures which we compare our 

 **- valid:** Valid attendees/employees

 **- samples:** Total atendees pictures. We compare this against the `valid` folder.

# AWS July Hacakthon Team 5 Members
Jacques Arroyo

Daniel Connelly

Carla Rubio

Cody Brown

Nadtakan Jones

# References

We use an open source [frontend](https://github.com/dabit3/appsync-image-rekognition) which
we modify.

# Considerations/Disclaimer

This a project was made in a limited timeframe. This project
should not be used without other means of identifying someone
to validate that a user is not gaming the system. For example,
a user may put a photo of an employee up at a camera and gain
access to a building they do not have access to.
